# dm_q3dm6
![](assets/images/banner.png)

`dm_q3dm6` is a [Half-Life 2: Deathmatch](http://store.steampowered.com/app/320/) map created from scratch by me, [Saucy](http://saucy.se), around the year 2005. It is based on the famous Quake 3 map of the same name (`q3dm6`)!

When creating the map I had no measurements other than my eyes. I was constantly comparing [SourceSDK](https://en.wikipedia.org/wiki/Source_(game_engine)#Source_SDK)-view with the real Quake 3-view of the map. Sadly I cannot remember how long it took to create it, but it sure took a while!

## Want to play the map?
In the folder `/assets/maps/working` lies a .bsp map named `dm_q3dm6_beta2.bsp`. Copy the .bsp file over to your Half-Life 2: Deathmatch maps folder (`/common/Half-Life 2 Deathmatch/hl2mp/maps`).

## Want to edit the source .vmf file?
Two things to consider: First, I was young (about 17 years old) "mapmaker". Second, there were **no version control** back in the day (there were lots of copies of files everywhere though!).

I _think_ I've included everything for you to be able to open up the map in the [Valve Hammer Editor](https://developer.valvesoftware.com/wiki/Valve_Hammer_Editor) and poke around in there. `q3dm6_beta2_v1` is most likely the latest version of the map I worked on, but like I said, I am not sure.

Have fun!

## License
[MIT License](LICENSE.md)
